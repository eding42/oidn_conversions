echo "denoising all images"

for file in ~/Desktop/senna_ishan_vr/*.pfm; do
 ./denoise -hdr $file -o $file.denoised.pfm -threads 14
 echo "Now denoising $file" 
done
shopt -s nullglob
