echo "denoising all images"

for file in *.pfm; do
 denoise -hdr $file -o $file.denoised.pfm -threads 14
 echo "Now denoising $"
done
shopt -s nullglob
